FROM node:latest as node
RUN mkdir /HN_ASALGADO

COPY server.js /HN_ASALGADO
COPY package.json /HN_ASALGADO
COPY launcher.js /HN_ASALGADO
COPY Route /HN_ASALGADO/Route
COPY MongoConfig /HN_ASALGADO/MongoConfig
COPY Models /HN_ASALGADO/Models
COPY Controller /HN_ASALGADO/Controller

WORKDIR /HN_ASALGADO
RUN npm install
EXPOSE 8080
CMD node server.js