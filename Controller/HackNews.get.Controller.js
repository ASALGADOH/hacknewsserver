const db = require("../MongoConfig/property");
var dbURL = require("../MongoConfig/connector");
const Schema = require("../Models/HackNews.model");
var chalk = require("chalk");
var connected = chalk.bold.cyan;
var error = chalk.bold.yellow;
var disconnected = chalk.bold.red;
var termination = chalk.bold.magenta;

exports.getNews = function(req, res) {
  try {
    const newsToSearch = req.query.news;
    if (newsToSearch === "all") {

        var findAll = new Promise((resolve, reject) => {
            conectar();
            Schema.find({}).sort([ ["created_at", -1.0] ])
                  .lean()
                  .exec(async (err, result) => {
                      if (err) {
                          error('ERROR al buscar en la coleccion');
                          res.send({
                              error: err,
                              message: 'Error al buscar en la colleccion'
                          });
                          res.end();
                          resolve('ERROR');
                      } else {
                        res.send(result);
                        res.end();
                        resolve('CORRECT');
                      }
                  })
        });

        findAll.then( prom => {
            if(prom === 'CORRECT') {
                console.log(`THE NEWS CONSULTATION ENDS SUCCESSFUL AT ${new Date()}` )
            } else {
                console.log(`THE NEWS INQUIRY ENDED IN ERROR AT ${new Date()}`)
            }
            desconectar();
        })

    } else {
      res.send("ERROR: Id de busqueda incorrecto");
      res.end();
    }
  } catch (err) {
    error(err);
  }
};

function conectar() {
  db.connect(dbURL, {
    useNewUrlParser: true
  });
}

function desconectar() {
  db.disconnect();
}
