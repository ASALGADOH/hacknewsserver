const db = require("../MongoConfig/property");
var dbURL = require("../MongoConfig/connector");
const axios = require("axios");
const Schema = require("../Models/HackNews.model");
var chalk = require("chalk");
var connected = chalk.bold.cyan;
var error = chalk.bold.yellow;
var disconnected = chalk.bold.red;
var termination = chalk.bold.magenta;

exports.fisrtload = function(req, res) {
  try {
    var id = req.query.id;
    console.log(id, " ID");

    if (id === "START") {
      axios
        .get("https://hn.algolia.com/api/v1/search_by_date?query=nodejs")
        .then(response => {
          conectar();
          conectionLogger();
          Schema.countDocuments()
            .lean()
            .exec(function(err, result) {
              /// console.log(result, "RECORDS RESULT");
              if (err) {
                console.log("ERROR ON COUNT FROM COLLECTION");
              }
              if (result === 0) {
                var insert = new Promise((resolve, reject) => {
                  response.data.hits.forEach(async (value, index, array) => {
                    await Schema.create(value)
                      .then((data, error) => {
                        if (data) {
                          console.log(`CREATION OF DOCUMENT COMPLETE ID:[${value.objectID}]`);
                        }
                        if(error) {
                          console.log(`ERROR ON CREATE DOCUMENT ID:[${value.objectID}]`)
                        }
                      })
                      .catch(err => {
                        console.log(err, " ERROR");
                      });
                    if (index === array.length - 1) resolve();
                  });
                });

                insert.then(() => {
                  res.send("FIRST LOAD DB COMPLETE [RUNNING ITERATOR]");
                  res.end();
                  desconectar();
                  iterateEveryHour();
                });
              } else {
                res.send(
                  "DB IS NOT EMPTY, INITIAL CHARGE IS NOT NECESARY [RUNNING ITERATOR]"
                );
                res.end();
                desconectar();
                iterateEveryHour();
              }
            });
        })
        .catch(error => {
          console.log(error);
        });
    } else {
      res.send(`PARAMETRO DE INICIO INGRESADO ES INVALIDO [${id}]`);
      res.end();
    }
  } catch (erorr) {
    res.send(error);
    res.end();
  }
};

function conectar() {
  db.connect(dbURL, {
    useNewUrlParser: true
  });
}

function conectionLogger() {
  db.connection.on("connected", function() {
    console.log(
      connected(
        "Mongoose default connection is open ",
        ` AT ${new Date()}`
      )
    );
  });

  db.connection.on("error", function(err) {
    console.log(
      error(
        "Mongoose default connection has occured " + err + " error",
        ` AT ${new Date()}`
      )
    );
  });
  db.connection.on("disconnected", function() {
    console.log(
      disconnected(
        "Mongoose default connection is disconnected",
        ` AT ${new Date()}`
      )
    );
  });
}

function desconectar() {
  db.disconnect();
}

function iterateEveryHour() {
  console.log('************* | STARTING COMPLETE | *************')
  console.log('************* | INITIATING ITERATOR | *************')
  function tick() {
    // get the mins of the current time
    var mins = new Date().getMinutes();
    var seconds = new Date().getSeconds();
    // EXECUTE AFTER ONE MINUTE IN NEW HOUR DETECTED
    if (mins == "00" && seconds == "30") {
        console.log("ITERATOR RUN AT " + new Date());
        axios
          .get("https://hn.algolia.com/api/v1/search_by_date?query=nodejs")
          .then(response => {
            var insert = new Promise((resolve, reject) => {
              conectar();
              response.data.hits.forEach(async (value, index, array) => {
                var query = {
                  objectID: value.objectID.toString()
                };
                await Schema.findOne(query)
                  .lean()
                  .exec(async (err, result) => {
                    if (result !== null) {
                      console.log(`UPDATING DOCUMENT IN ITERATOR [ID:${value.objectID}] `+
                      new Date());
                      await Schema.updateOne(
                        { objectID: result.objectID },
                        { $set: value },
                        function(err, result) {
                          if (err) {
                            console.error(
                              err,
                              `ERROR ON UPDATE IN ITERATOR [ID:${value.objectID}] ` +
                                new Date()
                            );
                          }
                          if (result) {
                            console.log(
                              `UPDATE COMPLETED OK IN ITERATOR [ID:${value.objectID}] ` +
                                new Date()
                            );
                          }
                        }
                      );
                    } else {
                      console.log(
                        `CREATING DOCUMENT IN ITERATOR [ID:${value.objectID}] ` +
                          new Date()
                      );
                      await Schema.create(value)
                        .then((data, error) => {
                          if (data) {
                            console.log(
                              `INSERT DOCUMENT OK IN ITERATOR [ID:${value.objectID}] ` +
                                new Date()
                            );
                          }
                          if (error) {
                            console.log(
                              err,
                              `ERROR ON CREATE DOCUMENT IN ITERATOR [ID:${value.objectID}] ` +
                                new Date()
                            );
                          }
                        })
                        .catch(err => {
                          console.log(err, " ERROR ");
                        });
                    }
                    if (index === array.length - 1) resolve();
                  });
              });
            });
    
            insert.then(() => {
              console.log("ITERATION COMPLETE AT " + new Date());
              desconectar();
            });
          })
          .catch(err => {
            console.error(err, " ERROR ON GET");
          });
    }
  }

  setInterval(function() {
    tick();
  }, 1000);
}
