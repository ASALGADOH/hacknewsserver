const mongoose = require('mongoose');
const notices = mongoose.Schema({
        created_at: Date,
        title: String,
        url: String,
        author: String,
        points: String,
        story_text: String,
        comment_text: String,
        num_comments: String,
        story_id: Number,
        story_title: String,
        story_url: String,
        parent_id: Number,
        created_at_i: Number,
        _tags: [],
        objectID: String,
        _highlightResult: {
          author: {
            value: String,
            matchLevel: String,
            matchedWords: []
          },
          comment_text: {
            value: String,
            matchLevel: String,
            fullyHighlighted: Boolean,
            matchedWords: []
          },
          story_title: {
            value: String,
            matchLevel: String,
            matchedWords: []
          }
        }
}, {
  timestamps: true
});
module.exports = mongoose.model('notices', notices);