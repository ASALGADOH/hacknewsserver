let express = require('express');
let bodyParser = require('body-parser');
let app = express();
let Routes = require("./Route/Route")
const cors = require('cors');


app.use(cors({ origin: true, credentials: true }));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.set('view engine', 'jade');
app.disable('etag');


var port = process.env.PORT || 8080;



app.get('/node/', (req, res) => {
    setTimeout(() => {
        res.send({ Status: 'Online', message: 'Generador de Apis Rest con Node.JS', Fecha: new Date() })
    })
})

app.use('/node/api', Routes)
app.listen(port, function() {
    console.log("Estoy corriendo en el puerto: " + port);
});