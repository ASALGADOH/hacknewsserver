let router = require('express').Router();
router.get('/', function(req, res) {
    res.json({
        status: 'API RUNNING',
        message: 'SERVICIO CREADO POR ALEX R. SALGADO H.',
    });
});

var initialConfig = require('../Controller/FirstLoadDBController');
var getAllNews = require('../Controller/HackNews.get.Controller');

router.route('/news').get(getAllNews.getNews);
router.route('/firstload').get(initialConfig.fisrtload);

module.exports = router;