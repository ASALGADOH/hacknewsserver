module.exports = {
    apps: [{
        name: "RestVisorResolver",
        script: "./server.js",
        watch: true,
        env: {
            "PORT": 5050,
            "NODE_ENV": "development"
        },
        env_production: {
            "PORT": 6060,
            "NODE_ENV": "production",
        }
    }]
}

/* ARCHIVO CONFIGURACION PM2 */